## What?

My solutions from the Qualifying round of [Google Code Jam 2019](https://codingcompetitions.withgoogle.com/codejam/round/0000000000051705).

## Solutions
### Problem 1: Forgone solution

The digit '4' cannot occur in the output, so the simplest solution is to find all positions in the number that hold a '4' and substract '1' from it.

```shell
$ python3 forgone-solution.py < Problem1.in
```

### Problem 2: You can go your own way

Try to walk het one of the two edges first. Choose to walk to the edge that is in the direction of the last step of Lydia. So if Lydia finished with a move 'S', try to walk to the south of the maze. In that case you can never have the las step the same as Lydia.

Meanwhile keep track of your position. Your moves are limited when

  - You are on the same coordinate as Lydia
  - When you are at the edge of the maze

```shell
$ python3 you-can-go-your-own-way.py < Problem2.in
```

### Problem 3: Cryptopangrams (Wrong answer)

Create a bunch of testdata

```shell
$ python3 data/cryptopangram_generator.py [--max-size=100] [--max-prime=10000] [--add-result > samples/testdata.txt

  --max-size     the size of the data set
  --max-prime    The max value for the prime numbers to use.
                 Does not have to be a prime number.
                 Wil automatically pick the largest that is smaller or equal to --max=prime
  --add-result   Add the result as third line to each output. For verifying purposes
                 Need to parse this correctly as well
```

You can add the result to the input as well, using the `--add-result` flag. However, then it needs to be passed to the program as well, so it knows that each third line is the expected output

Then Run it

```shell
$ python3 cryptopangrams.py [--verify] < samples/testdata.txt

  --verify   When the data is generated with the --add-result flag, --verify will force
             validation of the calculated result with the obtained result.
             When they differ, a message will be printed to STDERR
```

NOTE: Uncomment the lint that prints the output. Then use it to validate result
