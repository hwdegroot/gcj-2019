#!/bin/python
import sys
from os.path import (
    dirname,
    realpath,
    join as join_path
)
sys.path.append(dirname(realpath(join_path(__file__, '..'))))
import random, string, math, argparse
from cryptopangrams import (
    gen_primes,
    get_primes,
    unique
)

def gen_data(N, L, add_result = False):
    global PRIMES
    p = get_primes(N, PRIMES)

    s = ''.join([random.choice(string.ascii_uppercase) for i in range(L+1)])
    s += all_letters(s)
    primes = random_choices(p)
    primes.sort()

    mapper = dict((string.ascii_uppercase[i], primes[i]) for i in range(26))

    values = list(mapper[l] for l in s)
    x = []
    for i in range(1, len(values)):
        x.append(values[i-1] * values[i])
    print('{} {}'.format(N, L))
    print(' '.join([str(j) for j in x]))

    if add_result:
        print(s)


def all_letters(s):
    r = []
    for l in string.ascii_uppercase:
        if l not in s:
            r.append(l)

    return ''.join(r)

def random_choices(values):
    if len(values) == 26:
        return values

    choices = []
    while len(choices) < 26:
        n = random.randint(0, len(values)-1)
        if values[n] not in choices:
            choices.append(values[n])

    return choices

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create testdata set for cryptopangram')
    parser.add_argument('--size', type=int, default=100, help='size of the dataset')
    parser.add_argument('--max-prime', type=int, default=10000, help='Max value of prime number')
    parser.add_argument('--add-result', action='store_true', help='Print result as 3rd line')
    args = parser.parse_args()

    max_prime = args.max_prime if args.max_prime > 101 else 101
    dataset_size = args.size if args.size > 25 else 25
    PRIMES = gen_primes(max_prime)

    print(dataset_size)
    for x in range(dataset_size):
        gen_data(random.randint(101, max_prime), random.randint(25, dataset_size), args.add_result)
