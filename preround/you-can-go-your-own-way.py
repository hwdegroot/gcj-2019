#!/bin/python

def reader():
    nr_of_cases = int(input())
    cases = []
    for case_nr in range(nr_of_cases):
        steps = int(input())
        direction = str(input())
        cases.append([steps, direction])

    return [nr_of_cases, cases]

def solver(nr_of_cases, cases):
    result = []
    case_nr = 0
    for case in cases:
        case_nr += 1
        result.append(solve_case(case_nr, *case))

    return result

def printer(fmt, results):
    for result in results:
        for iteration, solution in result.items():
            print(fmt.format(iteration, solution))


def solve_case(case_nr, maze_size, lydias_path):
    result = ''.join(['S' if m == 'E' else 'E' for m in lydias_path])
    return {case_nr: result}


if __name__ == '__main__':
    cases = reader()
    results = solver(*cases)
    printer('Case #{}: {}', results)

