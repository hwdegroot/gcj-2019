#!/bin/python
import argparse, sys


def reader(verify = False):
    nr_of_cases = int(input())
    cases = []
    for case_nr in range(nr_of_cases):
        N,L = [int(s) for s in input().split(" ")]
        numbers = input().split(" ")
        case = {
            'N': N,
            'L': L,
            'ciphers': numbers
        }
        if verify:
            expected = input()
            case.update({
                'expected': expected
            })

        cases.append(case)

    return [nr_of_cases, cases]

def solver(nr_of_cases, cases):
    result = []
    case_nr = 0
    for case in cases:
        case_nr += 1
        result.append(solve_case(case_nr, case))

    return result

def printer(fmt, results):
    for result in results:
        for iteration, solution in result.items():
            print(fmt.format(iteration, solution))

def solve_case(case_nr, case):
    global PRIMES
    LETTERS ='ABCDEFGHIJKLMNOPQRSTUVWXYZ' # string.ascii_uppercase
    max_prime = int(case['N'])

    # Get the subset of a list of primes, that was generated
    # on the max prime number initially
    primes = get_primes(max_prime, PRIMES)
    ciphers = [int(c) for c in case['ciphers']]

    vals = list(find_first_prime_product(ciphers[0], primes))
    rvals = vals[::-1]
    for c in ciphers[1:]:
        vals.append(c/vals[-1])
        rvals.append(c/rvals[-1])


    uniq_values = unique(vals)
    values = vals
    if len(uniq_values) != 26:
        uniq_values = unique(rvals)
        values = rvals

    uniq_values.sort()

    text = []
    for val in values:
        text.append(LETTERS[uniq_values.index(val)])

    cipher_text = "".join(text)
    if 'expected' in case and cipher_text != case['expected']:
        print(
            "[ERROR] {}: Wrong result '{}' did not match expected '{}'".format(
                case_nr,
                cipher_text,
                case['expected']
            ),
            file=sys.stderr
        )

    return {case_nr: cipher_text}

def unique(duplicates):
    u = []
    for item in duplicates:
        if not item in u:
            u.append(item)
    return u

def find_first_prime_product(product, primes):
    for i in primes:
        for j in primes:
            if i * j == int(product):
                return i, j

def gen_primes(end):
    if end < 2: return []

    #The array doesn't need to include even numbers
    lng = int((end / 2) - 1 + end % 2)

    # Create array and assume all numbers in array are prime
    sieve = [True]*(lng + 1)

    # In the following code, you're going to see some funky
    # bit shifting and stuff, this is just transforming i and j
    # so that they represent the proper elements in the array.
    # The transforming is not optimal, and the number of
    # operations involved can be reduced.

    # Only go up to square root of the end
    for i in range(int((end ** 0.5)) >> 1):

      # Skip numbers that aren’t marked as prime
      if not sieve[i]: continue

      # Unmark all multiples of i, starting at i**2
      for j in range( (i*(i + 3) << 1) + 3, lng, (i << 1) + 3):
          sieve[j] = False

    # Don't forget 2!
    primes = [2]

    # Gather all the primes into a list, leaving out the composite numbers
    primes.extend([(i << 1) + 3 for i in range(lng) if sieve[i]])

    return primes

def get_primes(max_prime, primes):
    # return subset from primes smaller than max_prime
    if primes[-1] <= max_prime:
        return primes

    return [p for p in primes if p <= max_prime]

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Decipher cryptionpanrgams')
    parser.add_argument('--verify', action='store_true', help='Verify the calculated result')
    args = parser.parse_args()

    cases = reader(args.verify)
    MAX_PRIME = max([c['N'] for c in cases[1]])
    PRIMES = gen_primes(MAX_PRIME)
    results = solver(*cases)
    printer('Case #{}: {}', results)

