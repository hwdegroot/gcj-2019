#!/bin/python

def reader():
    nr_of_cases = int(input())
    cases = []
    for case_nr in range(nr_of_cases):
        cases.append(input())

    return [nr_of_cases, cases]

def solver(nr_of_cases, cases):
    result = []
    case_nr = 0
    for case in cases:
        case_nr += 1
        result.append(solve_case(case_nr, case))

    return result

def printer(fmt, results):
    for result in results:
        for iteration, solution in result.items():
            print(fmt.format(iteration, *solution))

def solve_case(case_nr, nr_jamcoins):
    result = "".join(
            [
                '1' if nr_jamcoins[pos] == '4' else '0'  for pos in range(len(nr_jamcoins))
            ]
        )

    return {case_nr: [int(result), int(nr_jamcoins) - int(result)]}


if __name__ == '__main__':
    cases = reader()
    results = solver(*cases)
    printer('Case #{}: {} {}', results)

